A Node JS app that allows a user to register and login with JWT, to create and access the notes.

- register and login with JWT authentication;
- ability to get and delete user's profile;
- ability to add a user's note, get/update/delete a user's note by id;

Database: MongoDB;
Tools: Mongoose, MongoDB Atlas, Node JS, Express, JWT, Swagger UI, morgan, bcrypt, eslint, postman
