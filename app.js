const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const User = require('./models/user');
const Note = require('./models/note');
const jwt = require('jsonwebtoken');
const cookieParser = require('cookie-parser');

mongoose.connect('mongodb+srv://kristik991:verySafePass@cluster0.0i8dl.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    {useNewUrlParser: true, useUnifiedTopology: true}).then(() => {
  console.log('MONGO CONNECTION OPEN!!!');
})
    .catch((err) => {
      console.log('OH NO MONGO CONNECTION ERROR!!!!');
      console.log(err);
    });

require('dotenv').config();

app.use(cookieParser());
app.use(cors());
app.use(morgan('tiny'));
app.use(express.json());

app.use('/api/notes/*', (req, res, next) => {
  const url = req.originalUrl;
  const word = url.split('/');
  const newW = word[word.length - 1];
  if (!newW) {
    return res.status(200).json({message: 'ID parameter required'});
  } else {
    next();
  }
});

app.post('/api/auth/register', async function createProfile(req, res) {
  let {username, password} = req.body;
  if (!username || !password) {
    res.status(400).json({message: 'Username and password are required'});
  }
  bcrypt.genSalt(10, function(err, salt) {
    if (err) throw err;
    bcrypt.hash(password, salt, function(err, hash) {
      password = hash;
    });
  });
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    const ifExistUser = await User.findOne({username: username});
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else if (ifExistUser) {
      db.close();
      return res.status(400).json({message: 'Username already exists'});
    } else {
      const date = new Date();
      const newUser = new User({
        username: username,
        password: password,
        createdDate: date.toString(),
      });
      await newUser.save();
      db.close();
      return res.status(200).json({message: 'Success'});
    }
  });
});

app.post('/api/auth/login', async function login(req, res) {
  const {username, password} = req.body;
  if (!username || !password) {
    res.status(400).json({message: 'Username and password are required'});
  }
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const hashedPassword = await User.findOne({username: username});
      if (!hashedPassword) {
        db.close();
        return res.status(400).json({message: 'Username does not exist'});
      }
      bcrypt.compare(password, hashedPassword.password, function(err, result) {
        if (!result) {
          db.close();
          return res.status(400).json({message: 'Password is incorrect'});
        } else if (result) {
          const token = jwt.sign({username}, process.env.TOKEN_KEY);
          db.close();
          return res.status(200).json({
            message: 'Success',
            jwt_token: `${token}`,
          });
        }
      });
    }
  });
});

const checkToken = (req, res, next) => {
  const token = req.headers.authorization;
  let key = token;
  if (token.split(' ')[0] === 'JWT' || token.split(' ')[0] === 'Bearer') {
    key = token.split(' ')[1];
  }
  if (!token) {
    return res.status(400).send('A token is required for authentication');
  }
  if (!req.params) {
    console.log(req.params);
    return res.status(200).json({message: 'ID parameter required'});
  }
  try {
    const decoded = jwt.verify(key, process.env.TOKEN_KEY);
    req.user = decoded;
  } catch (err) {
    return res.status(400).send('Invalid Token');
  }
  return next();
};

app.get('/api/users/me', checkToken, async function getProfileInfo(req, res) {
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({username: user.username});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        db.close();
        return res.status(200).json({
          user: {
            _id: userFromDB._id,
            username: userFromDB.username,
            createdDate: userFromDB.createdDate,
          },
        });
      }
    }
  });
});

app.delete('/api/users/me', checkToken,
    async function deleteProfile(req, res) {
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            const userFromDB = await User.findOne({username: user.username});
            if (!userFromDB) {
              db.close();
              return res.status(400).json({message: 'Username does not exist'});
            }
            await User.findOneAndDelete({username: user.username});
            await Note.deleteMany({userId: userFromDB._id});
            db.close();
            return res.status(200).json({message: 'Success'});
          }
        }
      });
    });

app.patch('/api/users/me', checkToken,
    async function changeProfilePassword(req, res) {
      let {oldPassword, newPassword} = req.body;
      if (!oldPassword || !newPassword) {
        res.status(400).json({
          message: 'oldPassword and newPassword are required',
        });
      }
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            const userFromDB = await User.findOne({username: user.username});
            if (!userFromDB) {
              db.close();
              return res.status(400).json({message: 'Username does not exist'});
            }
            bcrypt.compare(oldPassword, userFromDB.password,
                function(err, result) {
                  if (!result) {
                    db.close();
                    return res.status(400).json({
                      message: 'OldPassword is incorrect',
                    });
                  } else {
                    bcrypt.genSalt(10, function(err, salt) {
                      if (err) throw err;
                      bcrypt.hash(newPassword, salt, async function(err, hash) {
                        newPassword = hash;
                        const updatedUser = await User.findOneAndUpdate(
                            {username: user.username},
                            {password: newPassword},
                            {new: true},
                        );
                        console.log('updated user in the end: ', updatedUser);
                        db.close();
                        return res.status(200).json({message: 'Success'});
                      });
                    });
                  }
                });
          }
        }
      });
    });

app.get('/api/notes', checkToken, async function getUserNotes(req, res) {
  const {offset, limit} = req.query;
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({username: user.username});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        await Note.find({userId: userFromDB._id});
        const paginatedNotes = await Note.find({userId: userFromDB._id})
            .skip(+offset)
            .limit(+limit);
        db.close();
        return res.status(200).json({
          offset: +offset,
          limit: +limit,
          count: paginatedNotes.length,
          notes: await Note.find({userId: userFromDB._id})
              .skip(+offset)
              .limit(+limit),
        });
      }
    }
  });
});

app.post('/api/notes', checkToken, async function addUserNotes(req, res) {
  const {text} = req.body;
  if (!text) {
    res.status(400).json({message: 'Text is required'});
  }
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({username: user.username});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        const date = new Date();
        const newNote = new Note({
          userId: userFromDB._id,
          completed: false,
          text: text,
          createdDate: date.toString(),
        });
        await newNote.save();
        db.close();
        return res.status(200).json({message: 'Success'});
      }
    }
  });
});

app.get('/api/notes/:id', checkToken, async function getUserNoteById(req, res) {
const {id} = req.params;
  MongoClient.connect(process.env.DB_URI, async function(err, db) {
    if (err) {
      db.close();
      return res.status(500).json({message: 'Server error'});
    } else {
      const user = req.user;
      if (!user) {
        return res.status(400).json({message: 'Cannot find your token'});
      } else {
        const userFromDB = await User.findOne({username: user.username});
        if (!userFromDB) {
          db.close();
          return res.status(400).json({message: 'Username does not exist'});
        }
        try {
          const foundNote = await Note.findOne(
              {userId: userFromDB._id, _id: id});
          db.close();
          return res.status(200).json({
            note: {
              _id: foundNote._id,
              userId: foundNote.userId,
              completed: foundNote.completed,
              text: foundNote.text,
              createdDate: foundNote.createdDate,
            },
          });
        } catch (e) {
          db.close();
          return res.status(400).json({
            message: `A note with ID: ${id} does not exist`,
          });
        }
      }
    }
  });
});

app.put('/api/notes/:id', checkToken,
    async function updateUserNoteById(req, res) {
      const {id} = req.params;
      const {text} = req.body;
      if (!text) {
        res.status(400).json({message: 'Text is required'});
      }
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            const userFromDB = await User.findOne({username: user.username});
            if (!userFromDB) {
              db.close();
              return res.status(400).json({message: 'Username does not exist'});
            }
            try {
              await Note.findOne({userId: userFromDB._id, _id: id});
              await Note.findOneAndUpdate(
                  {userId: userFromDB._id, _id: id}, {text: text});
              db.close();
              return res.status(200).json({message: 'Success'});
            } catch (e) {
              db.close();
              return res.status(400).json({
                message: `A note with ID: ${id} does not exist`,
              });
            }
          }
        }
      });
    });

app.patch('/api/notes/:id', checkToken,
    async function toggleCompletedForUserNoteById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json({message: 'Cannot find your token'});
          } else {
            const userFromDB = await User.findOne({username: user.username});
            if (!userFromDB) {
              db.close();
              return res.status(400).json({message: 'Username does not exist'});
            }
            try {
              const foundNote = await Note.findOne({
                userId: userFromDB._id, _id: id});
              await Note.findOneAndUpdate({userId: userFromDB._id, _id: id},
                  {completed: !foundNote.completed}, {new: true});
              db.close();
              return res.status(200).json({message: 'Success'});
            } catch (e) {
              db.close();
              return res.status(400).json({
                message: `A note with ID: ${id} does not exist`,
              });
            }
          }
        }
      });
    });

app.delete('/api/notes/:id', checkToken,
    async function deleteUserNoteById(req, res) {
      const {id} = req.params;
      MongoClient.connect(process.env.DB_URI, async function(err, db) {
        if (err) {
          db.close();
          return res.status(500).json({message: 'Server error'});
        } else {
          const user = req.user;
          if (!user) {
            return res.status(400).json(
                {message: 'Cannot find your token'});
          } else {
            const userFromDB = await User.findOne(
                {username: user.username});
            if (!userFromDB) {
              db.close();
              return res.status(200).json(
                  {message: 'Username does not exist'});
            }
            try {
              await Note.findOne(
                  {userId: userFromDB._id, _id: id});
              await Note.findOneAndDelete(
                  {userId: userFromDB._id, _id: id});
              db.close();
              return res.status(200).json({message: 'Success'});
            } catch (err) {
              db.close();
              return res.status(200).json({
                message: `A note with ID: ${id} does not exist`,
              });
            }
          }
        }
      });
    });

app.listen(8080, () => {
  console.log('LISTENING ON PORT 8080');
});
